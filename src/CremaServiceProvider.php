<?php

declare(strict_types = 1);

namespace Drupal\crema;

use Composer\Autoload\ClassLoader;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DrupalKernelInterface;
use Drupal\Core\Extension\InfoParserInterface;

/**
 * Service provider of Crema.
 *
 * Discovers replaced and replacement classes, then creates and registers
 * their special class loader.
 */
class CremaServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $kernel = $container->get('kernel');
    assert($kernel instanceof DrupalKernelInterface);
    $modules = $container->getParameter('container.modules');
    $info_parser = $container->get('info_parser');
    assert($info_parser instanceof InfoParserInterface);
    $all_replacements = array_reduce(
      $modules,
      function (array $carry, array $module_info) use ($info_parser) {
        $info = $info_parser->parse($module_info['pathname']);
        if (empty($replacements = ($info['class_replacements'] ?? []))) {
          return $carry;
        }
        $dir = dirname($module_info['pathname']);
        array_walk(
          $replacements,
          function (&$relative_path, $key, $extension_dir) {
            $relative_path = $extension_dir . DIRECTORY_SEPARATOR . $relative_path;
          },
          $dir
        );
        $carry = array_merge($carry, $replacements);
        return $carry;
      },
      []
    );

    // Return as early as possible if there are no replaced classes.
    if (empty($all_replacements)) {
      return;
    }

    $app_root = $kernel->getAppRoot();
    $originals = array_reduce(
      array_keys($all_replacements),
      function (array $carry, string $replaced) use ($modules, $app_root) {
        if (preg_match('/^Drupal\\\([^\\\]*)\\\/', $replaced, $matches)) {
          if (isset($modules[$matches[1]])) {
            $dir = dirname($modules[$matches[1]]['pathname']);
            $carry["Drupal\\" . CremaClassLoader::ORIGINAL_SUBNAMESPACE . "\\{$matches[1]}"] = $app_root . DIRECTORY_SEPARATOR . $dir . '/src';
          }
        }
        return $carry;
      },
      []
    );
    $replacement_loader = static::createCremaClassLoader($kernel, $all_replacements, $originals);
    $replacement_loader->register(TRUE);
  }

  /**
   * Creates a new CremaClassLoader instance based on the one in DrupalKernel.
   *
   * @param \Drupal\Core\DrupalKernelInterface $kernel
   *   The Drupal kernel.
   * @param array $all_replacements
   *   List of the path of the replacement classes, keyed by the class' original
   *   fully-qualified name.
   * @param array $originals
   *   List of the original classes.
   *
   * @return \Drupal\crema\CremaClassLoader
   *   The new CremaClassLoader instance ready to be registered.
   */
  protected static function createCremaClassLoader(DrupalKernelInterface $kernel, array $all_replacements, array $originals) {
    $kernel_ref = new \ReflectionClass($kernel);
    $original_classloader_ref_prop = $kernel_ref->getProperty('classLoader');
    $original_classloader_ref_prop->setAccessible(TRUE);
    $original_classloader = $original_classloader_ref_prop->getValue($kernel);
    assert($original_classloader instanceof ClassLoader);

    $original_classloader_ref = new \ReflectionClass($original_classloader);
    $vendor_dir_ref_prop = $original_classloader_ref->getProperty('vendorDir');
    $vendor_dir_ref_prop->setAccessible(TRUE);
    $prefixLengthsPsr4_ref_m = $original_classloader_ref->getProperty('prefixLengthsPsr4');
    $prefixLengthsPsr4_ref_m->setAccessible(TRUE);
    $prefixDirsPsr4_ref_m = $original_classloader_ref->getProperty('prefixDirsPsr4');
    $prefixDirsPsr4_ref_m->setAccessible(TRUE);

    $replacement_loader = new CremaClassLoader(
      $vendor_dir_ref_prop->getValue($original_classloader),
      $all_replacements,
      $originals
    );
    $replacement_loader->addPrefixLengthsPsr4($prefixLengthsPsr4_ref_m->getValue($original_classloader))
      ->addPrefixDirsPsr4($prefixDirsPsr4_ref_m->getValue($original_classloader))
      ->addPrefixes($original_classloader->getPrefixes())
      ->addFallbackDirsPsr4($original_classloader->getFallbackDirsPsr4())
      ->addFallbackDirs($original_classloader->getFallbackDirs());

    $replacement_loader->addClassMap($original_classloader->getClassMap());
    $replacement_loader->setClassMapAuthoritative($original_classloader->isClassMapAuthoritative());

    return $replacement_loader;
  }

}
