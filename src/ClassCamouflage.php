<?php

declare(strict_types = 1);

namespace Drupal\crema;

use Drupal\crema\Utility\ClassCamouflageTokenParser;

/**
 * A PHP class camouflage object.
 *
 * @internal
 */
final class ClassCamouflage {

  /**
   * Simplifed tokens of the actual class camouflage.
   *
   * @var array
   */
  protected $tokens;

  /**
   * Constructs a new ClassCamouflage instance.
   *
   * @param string $file_content
   *   The content of a PHP class|trait|interface file. The file must begin with
   *   a PHP open tag, it must have namespace and must contain one (and only
   *   one) class|trait|interface.
   */
  public function __construct(string $file_content) {
    $tokens_with_class_tokens = ClassCamouflageTokenParser::getSimplifiedTokens($file_content);
    foreach ($tokens_with_class_tokens as $key => $token_s) {
      if (is_string($key)) {
        $this->classesTokens[$key] = $token_s;
        $this->tokens = array_merge(
          $this->tokens ?? [],
          $token_s
        );
      }
      else {
        $this->tokens[] = $token_s;
      }
    }
  }

  /**
   * Creates a ClassCamouflage instance.
   *
   * @param string $filepath
   *   The path of a PHP class|trait|interface file. The file must begin with
   *   a PHP open tag, it must have namespace and must contain one (and only
   *   one) class|trait|interface.
   */
  public static function create(string $filepath) {
    return new static(
      file_get_contents($filepath)
    );
  }

  /**
   * Returns the declared classes and interfaces in the global ns.
   *
   * @return string[]
   *   The declared classes and interfaces in the global ns.
   */
  public function getBuiltinClasses(): array {
    foreach (get_declared_interfaces() as $interface) {
      if (!strpos($interface, '\\')) {
        $built_in_classes[] = $interface;
      }
    }

    foreach (get_declared_classes() as $class) {
      if (!strpos($class, '\\')) {
        $built_in_classes[] = $class;
      }
    }

    return $built_in_classes;
  }

  /**
   * Returns the new content of the class.
   *
   * @return string
   *   The content of the camouflaged new class.
   */
  public function getNew(): string {
    $new_tokens = $this->tokens;

    $namespace_control_index = ClassCamouflageTokenParser::getTokenIndex($new_tokens, T_NAMESPACE);
    $original_namespace = ClassCamouflageTokenParser::getNameSpace($new_tokens);
    $new_namespace = str_starts_with($original_namespace, 'CremaDropThis')
      ? preg_replace('/^CremaDropThis\b\\\\(.+)$/', "$1", $original_namespace)
      : preg_replace('/^Crema\b(.+)$/', "Drupal$1", $original_namespace);
    // Replace namespace.
    $new_tokens[$namespace_control_index + 2] = $new_namespace;

    foreach ($this->classesTokens as $new_class_tokens) {
      $name = ClassCamouflageTokenParser::getName($new_class_tokens);
      $original_class_usement_index = ClassCamouflageTokenParser::getTokenIndex($new_tokens, $new_namespace . '\\' . $name);
      if ($original_class_usement_index) {
        $supressed_class_usement = self::getSuppressedQcnName($new_namespace . '\\' . $name);
        $new_tokens[$original_class_usement_index] = $supressed_class_usement;
      }
    }

    return ClassCamouflageTokenParser::tokensToString($new_tokens);
  }

  /**
   * Returns the camouflaged content of the class.
   *
   * @return string
   *   The content of the camouflaged original class.
   */
  public function getCamouflaged(): string {
    $camouflage_tokens = $this->tokens;

    $original_namespace = ClassCamouflageTokenParser::getNameSpace($camouflage_tokens);

    // Change the namespace to Drupal\_original_\Foo\Bar\baz.
    $namespace_index = ClassCamouflageTokenParser::getTokenIndex($camouflage_tokens, T_NAMESPACE);
    $camouflage_tokens[$namespace_index + 2][1] = self::getSuppressedQcnName($camouflage_tokens[$namespace_index + 2][1]);

    // We inspect the use statements and the used classes:
    // If the original file tried to use (extend, implement etc) an another
    // class from the same original namespace, then we will add the proper use
    // statement.
    $classes_in_use_statements =
      array_merge(
        [$name = ClassCamouflageTokenParser::getName($camouflage_tokens)],
        ClassCamouflageTokenParser::getClassNamesFromUseStatements($camouflage_tokens)
    );
    $classes_with_direct_usage = ClassCamouflageTokenParser::getClassNamesWithDirectUsage($camouflage_tokens);
    foreach ($this->classesTokens as $class_tokens) {
      $extended_implemented_and_used_traits = array_unique(
        array_merge(
          $extended_implemented_and_used_traits ?? [],
          ClassCamouflageTokenParser::getExtendedImplemented($class_tokens)
        )
      );
    }

    $classes_without_use_statements = array_diff(
      array_unique(
        array_merge(
          $extended_implemented_and_used_traits,
          $classes_with_direct_usage
        )
      ),
      $classes_in_use_statements
    );
    // It is relatively expensive calling ::getBuiltinClasses, so do it only
    // when needed.
    if (!empty($classes_without_use_statements)) {
      $classes_without_use_statements = array_diff(
        $classes_without_use_statements,
        $this->getBuiltinClasses()
      );
    }

    if (!empty($classes_without_use_statements)) {
      $missing = array_reduce(
        $classes_without_use_statements,
        function (array $carry, string $missing) use ($original_namespace) {
          $carry[] = "use " . $original_namespace . "\\" . $missing . ";";
          return $carry;
        },
        [],
      );

      $semic_after_namespace_index = ClassCamouflageTokenParser::getFirstIndexAfter($camouflage_tokens, $namespace_index + 2, ';');
      $tokens_between_ns_semic_and_name = ClassCamouflageTokenParser::getTokensBetween(
        $camouflage_tokens,
        $semic_after_namespace_index,
        ClassCamouflageTokenParser::getNameIndex($camouflage_tokens)
      );
      $first_use_index_after_ns_before_name = ClassCamouflageTokenParser::getTokenIndex(
        $tokens_between_ns_semic_and_name,
        T_USE
      );

      $slice_index = $first_use_index_after_ns_before_name ?: $semic_after_namespace_index + 1;

      $strings_to_add = array_filter([
        $first_use_index_after_ns_before_name ? NULL : "\n\n",
        implode("\n", $missing),
        $first_use_index_after_ns_before_name ? "\n" : NULL,
      ]);

      $camouflage_tokens = array_merge(
        array_slice($camouflage_tokens, 0, $slice_index),
        $strings_to_add,
        array_slice($camouflage_tokens, $slice_index)
      );
    }

    // Replace every occurrence of the original relative self class name with a
    // fully qualified class name.
    foreach ($classes_with_direct_usage as $key => $cn) {
      if ($cn === $name) {
        $camouflage_tokens[$key] = '\\' . $original_namespace . '\\' . $name;
      }
    }

    // If there are classes in the file, then we must ensure they are declared
    // as abstract.
    if ($class_index = ClassCamouflageTokenParser::getNameIndex($camouflage_tokens, T_CLASS)) {
      $remaining_tokens = $camouflage_tokens;
      do {
        $keyword_index = ClassCamouflageTokenParser::getKeyWordIndexBefore($camouflage_tokens, $class_index);
        if ($keyword_index === NULL) {
          // We shouldn't modify the indexes.
          $camouflage_tokens[$class_index] = 'abstract class';
        }
        else {
          $camouflage_tokens[$keyword_index] = [
            T_ABSTRACT,
            'abstract',
          ];
        }

        $remaining_tokens = ClassCamouflageTokenParser::getTokensBetween($remaining_tokens, $class_index + 1);
      } while ($class_index = ClassCamouflageTokenParser::getNameIndex($remaining_tokens, T_CLASS));
    }

    return ClassCamouflageTokenParser::tokensToString($camouflage_tokens);
  }

  /**
   * Returns the original (suppressed) class' qualified name.
   *
   * @param string $default_qcn
   *   The default qualified class name of the replaces class.
   *
   * @return string
   *   The (suppressed) class' qualified name.
   */
  private static function getSuppressedQcnName($default_qcn): string {
    return preg_replace(
      '/^([\w]*)(\\\\.*)$/',
      '$1\\\\' . CremaClassLoader::ORIGINAL_SUBNAMESPACE . '$2',
      $default_qcn
    );
  }

}
