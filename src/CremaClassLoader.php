<?php

declare(strict_types = 1);

namespace Drupal\crema;

use Composer\Autoload\ClassLoader;

/**
 * Class loader for class replacements and for the replaced (original) classes.
 *
 * @internal
 */
final class CremaClassLoader extends ClassLoader {

  /**
   * The namespace piece Crema inserts between 'Drupal' and the module's name.
   *
   * E.g. if one overrides 'Drupal\example\ExampleClass', the we will make the
   * original class available with 'Drupal\_original_\example\ExampleClass',
   * which means that the original class can be extended by the replacer class.
   *
   * @const string
   */
  const ORIGINAL_SUBNAMESPACE = '_original_';

  /**
   * Stores the info about the replaced and the replacement files.
   *
   * Key is the replaced (original) file's qualified class name, the value is
   * the location of the replacement class, relative to the document root.
   *
   * @var string[]
   */
  protected $info;

  /**
   * Mapping of the dynamically constructed 'Drupal\_original_\*' namespaces.
   *
   * Key is the replaced (original) files new namespace, the value is
   * the location of the corresponding absolute src directory (assuming PSR-4).
   *
   * @var string[]
   */
  protected $originals;

  /**
   * Constructs a new CremaClassLoader.
   *
   * @param string|null $vendor_dir
   *   Path to the vendor directory root.
   * @param array $replacements
   *   Info about the replaced and the replacement files. Keys are the replaced
   *   (original) file's qualified class name, values are the location of the
   *   replacement class, relative to the document root.
   * @param array $originals
   *   Mapping of the dynamically constructed 'Drupal\_original_\*' namespaces.
   */
  public function __construct($vendor_dir = NULL, array $replacements = [], array $originals = []) {
    parent::__construct($vendor_dir);
    $this->info = $replacements;
    $this->originals = $originals;
    foreach ($originals as $original => $src) {
      $this->prefixLengthsPsr4[$original[0]][$original . "\\"] = strlen($original) + 1;
      $this->prefixDirsPsr4[$original . "\\"] = [$src];
    }
  }

  /**
   * Registers this autoloader instance.
   *
   * @param bool $prepend
   *   Whether to prepend the autoloader or not.
   */
  public function register($prepend = FALSE) {
    spl_autoload_register([$this, 'loadClass'], TRUE, $prepend);
  }

  /**
   * Unregisters this autoloader instance.
   */
  public function unregister() {
    spl_autoload_unregister([$this, 'loadClass']);
  }

  /**
   * Loads the given class or interface.
   *
   * @return true|null
   *   TRUE if the given class is handled by this class loader, the file was
   *   found and was included, or NULL if one of the above isn't true.
   */
  public function loadClass($name): ?bool {
    if (isset($this->info[$name])) {
      if ($this->findCamouflageFile($name)) {
        // Do loading.
        _crema_include_replacement_file($this->info[$name]);
        return TRUE;
      }
      else {
        throw new \LogicException('Cannot find replacement class');
      }
    }

    if (preg_match('/^[^\\\]+\b\\\\' . '_original_' . '\\\([^\\\]*)\\\.*/', $name)) {
      $orig = preg_replace('/(^[^\\\]+\b)\\\\' . '_original_' . '(\\\([^\\\]*)\\\.*)/', '$1$2', $name);
      if ($file = $this->findCamouflageFile($name, TRUE)) {
        _crema_include_original_file($file);
        return TRUE;
      }
      elseif ($file = $this->findFile($orig)) {
        _crema_include_original_file($file);
        return TRUE;
      }
      else {
        throw new \LogicException('Cannot include original class');
      }
    }
    return NULL;
  }

  /**
   * Finds a file by class name.
   *
   * The assumed file extension is '.php'.
   *
   * @return string|null
   *   The path of the given PHP class' file, or NULL if it cannot be found.
   */
  public function findCamouflageFile($name, $original = FALSE): ?string {
    if ($original && ($file = $this->findCamouflagedOriginalFile($name))) {
      return $file;
    }
    if (isset($this->info[$name]) && file_exists($this->info[$name])) {
      return $this->info[$name];
    }
    return NULL;
  }

  /**
   * Finds the original file by PSR-4 lookup.
   *
   * @param string $class
   *   The qualified class name, with '_original_' at the second level.
   *
   * @return string|null
   *   Returns the file path if the original class file was found, or NULL.
   *
   * @see \Composer\Autoload\ClassLoader::findFileWithExtension
   */
  private function findCamouflagedOriginalFile($class): ?string {
    $logical_psr4_path = strtr($class, '\\', DIRECTORY_SEPARATOR) . '.php';

    $first = $class[0];
    if (isset($this->prefixLengthsPsr4[$first])) {
      $subPath = $class;
      while (($lastPos = strrpos($subPath, '\\')) !== FALSE) {
        $subPath = substr($subPath, 0, $lastPos);
        $search = $subPath . '\\';
        if (isset($this->prefixDirsPsr4[$search])) {
          $pathEnd = DIRECTORY_SEPARATOR . substr($logical_psr4_path, $lastPos + 1);
          foreach ($this->prefixDirsPsr4[$search] as $dir) {
            if (file_exists($file = $dir . $pathEnd)) {
              return $file;
            }
          }
        }
      }
    }

    return NULL;
  }

  /**
   * Populates prefixLengthsPsr4 property directly.
   *
   * @param int[][] $psr4_prefix_lengths
   *   List of PSR4 namespace lengths, keyed by the first letter of the
   *   namespaces, then the namespace itself.
   *
   * @return self
   *   The current CremaClassLoader.
   */
  public function addPrefixLengthsPsr4(array $psr4_prefix_lengths) {
    foreach ($psr4_prefix_lengths as $first_letter => $lengths) {
      foreach ($lengths as $prefix => $length) {
        $this->prefixLengthsPsr4[$first_letter][$prefix] = $length;
      }
    }
    return $this;
  }

  /**
   * Populates prefixDirsPsr4 property directly.
   *
   * @param array[] $psr4_prefix_directories
   *   List of PSR4 prefix directories, keyed by prefix.
   *
   * @return self
   *   The current CremaClassLoader.
   */
  public function addPrefixDirsPsr4(array $psr4_prefix_directories) {
    foreach ($psr4_prefix_directories as $prefix => $dirs) {
      $this->prefixDirsPsr4[$prefix] = array_unique(
        array_merge(
          $this->prefixDirsPsr4[$prefix] ?? [],
          $dirs
        )
      );
    }
    return $this;
  }

  /**
   * Populates prefixesPsr0 property directly.
   *
   * @param array[] $psr0_prefixes
   *   List of PSR0 prefixes, keyed by prefix.
   *
   * @return self
   *   The current CremaClassLoader.
   */
  public function addPrefixes(array $psr0_prefixes) {
    foreach ($psr0_prefixes as $prefix => $dirs) {
      $this->prefixesPsr0[$prefix[0]][$prefix] = array_unique(
        array_merge(
          $this->prefixesPsr0[$prefix[0]][$prefix] ?? [],
          $dirs
        )
      );
    }
    return $this;
  }

  /**
   * Populates fallbackDirsPsr4 property directly.
   *
   * @param array[] $psr4_fallback_directories
   *   List of PSR4 fallback directories.
   *
   * @return self
   *   The current CremaClassLoader.
   */
  public function addFallbackDirsPsr4(array $psr4_fallback_directories) {
    $this->fallbackDirsPsr4 = array_unique(
      array_merge(
        $this->fallbackDirsPsr4 ?? [],
        $psr4_fallback_directories
      )
    );
    return $this;
  }

  /**
   * Populates fallbackDirsPsr0 property directly.
   *
   * @param array[] $psr0_fallback_directories
   *   List of PSR0 fallback directories.
   *
   * @return self
   *   The current CremaClassLoader.
   */
  public function addFallbackDirs(array $psr0_fallback_directories) {
    $this->fallbackDirsPsr0 = array_unique(
      array_merge(
        $this->fallbackDirsPsr0 ?? [],
        $psr0_fallback_directories
      )
    );
    return $this;
  }

}

/**
 * Scope isolated include.
 *
 * Prevents access to $this/self from included files.
 *
 * @param string $file
 *   The path of the file to include.
 *
 * @private
 */
function _crema_include_file($file): void {
  include $file;
}

/**
 * Includes the PHP class file which is replacing the original one.
 *
 * @param string $file
 *   The path of the file to include.
 *
 * @private
 */
function _crema_include_replacement_file($file): void {
  $camouflage = ClassCamouflage::create($file);

  $stream = tmpfile();
  fwrite($stream, $camouflage->getNew());
  rewind($stream);
  _crema_include_file(stream_get_meta_data($stream)['uri']);
  fclose($stream);
}

/**
 * Includes the original PHP file which was replaced.
 *
 * Danger zone!
 *
 * @param string $file
 *   The path of the file to include.
 *
 * @see https://youtu.be/kyAn3fSs8_A
 *
 * @private
 */
function _crema_include_original_file($file): void {
  $camouflage = ClassCamouflage::create($file);

  $stream = tmpfile();
  fwrite($stream, $camouflage->getCamouflaged());
  rewind($stream);
  _crema_include_file(stream_get_meta_data($stream)['uri']);
  fclose($stream);
}
