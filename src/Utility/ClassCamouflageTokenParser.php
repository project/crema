<?php

declare(strict_types = 1);

namespace Drupal\crema\Utility;

/**
 * Utility for inspecting tokens built from PHP class file contents.
 *
 * @internal
 */
class ClassCamouflageTokenParser {

  /**
   * Argument types which aren't class names.
   *
   * It is funny to see that 'array' has a token type, so it shouldn't be here.
   *
   * @const string[]
   */
  const ARG_TYPES = [
    'bool',
    'boolean',
    'float',
    'int',
    'iterable',
    'mixed',
    'null',
    'object',
    'parent',
    'self',
    'string',
  ];

  /**
   * The token constants of the class type control words.
   *
   * @todo Find out how (and when) to support 'T_ENUM'.
   *
   * @const int[]
   */
  const NAME_TOKENS = [
    T_CLASS,
    T_INTERFACE,
    T_TRAIT,
  ];

  /**
   * Tokenizes the given PHP class file and returns a simplified token map.
   *
   * @param string $class_file_content
   *   The file to tokenize.
   *
   * @return array
   *   The simplified token map of the given file.
   */
  public static function getSimplifiedTokens(string $class_file_content): array {
    $prev_was_skipped = FALSE;
    $namespace_found = FALSE;
    $names = [];
    $class_started = FALSE;
    $class_count = 0;
    $in_class_body = FALSE;
    $curly_depth_in_class_body = 0;
    $all_tokens = TokenParserShim::massageTokens(token_get_all($class_file_content));
    if ($all_tokens[0][0] !== T_OPEN_TAG) {
      throw new \LogicException('The tokenized PHP class must begin with a PHP open tag.');
    }

    foreach ($all_tokens as $in => $token) {
      $last_curly = FALSE;
      if (is_array($token)) {
        if (in_array($token[0], [T_COMMENT, T_DOC_COMMENT], TRUE)) {
          $prev_was_skipped = TRUE;
          continue;
        }
        if ($token[0] === T_WHITESPACE && $prev_was_skipped) {
          continue;
        }

        if ($token[0] === T_NAMESPACE) {
          $namespace_found = TRUE;
        }

        if (
          $namespace_found &&
          in_array($token[0], self::NAME_TOKENS) &&
          // Previous token is not a '::' string.
          $all_tokens[$in - 1][0] !== T_DOUBLE_COLON
        ) {
          $class_count++;
          $class_started = $all_tokens[$in + 2][1] . "_$class_count";
          $names[] = $all_tokens[$in + 2][1];
        }
      }
      elseif ($class_started) {
        if ($token === '{' && !$in_class_body && !$curly_depth_in_class_body) {
          $in_class_body = TRUE;
        }
        elseif ($token === '{' && $in_class_body) {
          $curly_depth_in_class_body++;
        }
        elseif ($token === '}' && $in_class_body && $curly_depth_in_class_body) {
          $curly_depth_in_class_body--;
        }
        elseif ($token === '}' && $in_class_body && !$curly_depth_in_class_body) {
          $in_class_body = FALSE;
          $last_curly = TRUE;
        }
      }

      if ($class_started) {
        $simplified_tokens[$class_started][] = $token;
        if ($last_curly) {
          $class_started = FALSE;
        }
      }
      else {
        $simplified_tokens[] = $token;
      }
      $prev_was_skipped = FALSE;
    }

    if (!$namespace_found) {
      throw new \LogicException('The tokenized PHP class must have a namespace.');
    }

    if (empty($names)) {
      throw new \LogicException('The tokenized PHP class does not contain any class name.');
    }

    return $simplified_tokens;
  }

  /**
   * Returns the class names or class aliases which have use statements.
   *
   * @param array $tokens
   *   The tokens to search in.
   * @param bool $scope_is_class_body
   *   Whether the scope should be the body of a class. Defaults to FALSE.
   *
   * @return string[]
   *   The class names or class aliases which have use statements. These
   *   probably have different namespace.
   */
  public static function getClassNamesFromUseStatements(array $tokens, bool $scope_is_class_body = FALSE): array {
    $search_end = $scope_is_class_body
      ? NULL
      : self::getNameIndex($tokens);
    $class_use_statements = array_filter(
      self::getTokensBetween($tokens, 0, $search_end),
      function ($token, $key) use ($tokens) {
        $prev_non_whitespace = ($tokens[$key - 1][0] ?? NULL) === T_WHITESPACE
          ? $tokens[$key - 2] ?? NULL
          : $tokens[$key - 1] ?? NULL;
        return $token[0] === T_USE &&
          in_array($prev_non_whitespace, [';', '{', '}', NULL], TRUE);
      },
      ARRAY_FILTER_USE_BOTH
    );

    foreach (array_keys($class_use_statements) as $t_use_key) {
      $tokens_between_use_and_semic = static::getTokensBetween($tokens, $t_use_key, ';');
      $t_as_index = static::getTokenIndex($tokens_between_use_and_semic, T_AS);
      $used_classes[] = $t_as_index && $t_as_index === $t_use_key + 4
        ? $tokens_between_use_and_semic[$t_as_index + 2]
        : $tokens_between_use_and_semic[$t_use_key + 2];
    }

    return array_reduce(
      $used_classes ?? [],
      function (array $carry, $item) {
        $used_parts = explode('\\', $item[1]);
        $carry[] = end($used_parts);
        return $carry;
      },
      []
    );
  }

  /**
   * Determines whether the current token may be a class reference in code.
   *
   * @param array $tokens
   *   The surrounding tokens.
   * @param int $key
   *   The key of the current T_STRING token.
   *
   * @return bool
   *   Whether the token may be a class reference in code.
   */
  protected static function mayBeRelativeClassNameReference(array $tokens, int $key): bool {
    if ($key === 0 && ($tokens[$key + 1][0] ?? NULL) === T_DOUBLE_COLON) {
      return TRUE;
    }

    $next_non_ws_token = $tokens[$key + 1][0] === T_WHITESPACE
      ? $tokens[$key + 2] ?? NULL
      : $tokens[$key + 1] ?? NULL;
    $prev_token = $tokens[$key - 1] ?? NULL;

    return $prev_token[0] !== T_NS_SEPARATOR &&
      (
        $next_non_ws_token[0] === T_DOUBLE_COLON ||
        (is_string($next_non_ws_token) && $next_non_ws_token === '(')
      );
  }

  /**
   * Determines whether the current token is a type declaration.
   *
   * @param array $tokens
   *   The surrounding tokens.
   * @param int $token_key
   *   The key of the current T_STRING token.
   *
   * @return bool
   *   Whether the current token is a type declaration.
   */
  protected static function tokenIsRelativeTypeDeclaration(array $tokens, int $token_key): bool {
    if (($tokens[$token_key - 1][0] ?? NULL) === T_NS_SEPARATOR) {
      return FALSE;
    }
    // E.g. "function(Foobar $foobar){}", but not "function(\Foobar $foobar({}".
    if (
      ($tokens[$token_key + 1][0] ?? NULL) === T_WHITESPACE &&
      ($tokens[$token_key + 2][0] ?? NULL) === T_VARIABLE
    ) {
      return TRUE;
    }
    // Token is the part of a union type declaration, and it isn't the last
    // one: "function(Foobar|string $foobar){}".
    if (
      ($tokens[$token_key + 1] ?? NULL) === '|' ||
      (
        ($tokens[$token_key + 1][0] ?? NULL) === T_WHITESPACE &&
        ($tokens[$token_key + 2] ?? NULL) === '|'
      )
    ) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns classes used in variable assignments, function arguments.
   *
   * @param array $tokens
   *   The tokens to search in.
   *
   * @return string[]
   *   The name of the classes which are used in variable assignments, function
   *   arguments etc.
   */
  public static function getClassNamesWithDirectUsage(array $tokens): array {
    $du_classnames = [];
    foreach ($tokens as $key => $token) {
      if ($token[0] !== T_STRING) {
        continue;
      }

      if (!ctype_upper($token[1][0])) {
        continue;
      }

      if (self::mayBeRelativeClassNameReference($tokens, $key)) {
        $du_classnames[$key] = $token[1];
      }
      elseif (($tokens[$key - 2][0] ?? NULL) === T_INSTANCEOF) {
        // Token is used in an instanceof check.
        $du_classnames[$key] = $token[1];
      }
      // Token is a single type declaration (in function argument).
      elseif (self::tokenIsRelativeTypeDeclaration($tokens, $key)) {
        $du_classnames[$key] = $token[1];
      }
    }

    return $du_classnames;
  }

  /**
   * Returns the key of the first token which follows the given token.
   *
   * @param array $tokens
   *   The tokens to search in.
   * @param int $search_start_index
   *   The index of the token where the search should start.
   * @param int|string $needle
   *   The token constant (integer) or the string value of the token whose first
   *   occurrence should be returned.
   *
   * @return int|null
   *   The index of the found token needle, or NULL if it cannot be found.
   */
  public static function getFirstIndexAfter(array $tokens, int $search_start_index, $needle): ?int {
    $search_key_pos = array_search($search_start_index, array_keys($tokens), TRUE);
    $tokens_from_ns = array_slice($tokens, $search_key_pos + 1, NULL, TRUE);
    // Needle is either a string (e.g. '{', ';'), or a token type (e.g. T_USE)
    // integer.
    if (is_string($needle)) {
      $first_index_after_start = array_search($needle, $tokens_from_ns, TRUE);
      return $first_index_after_start !== FALSE
        ? $first_index_after_start
        : NULL;
    }

    while (key($tokens_from_ns) !== NULL) {
      $current = current($tokens_from_ns);
      if (is_array($current) && $current[0] === $needle) {
        break 1;
      }
      next($tokens_from_ns);
    }

    return key($tokens_from_ns);
  }

  /**
   * Returns the extended, implemented classes and the traits used in the class.
   *
   * @param array $tokens
   *   The tokens to search in.
   *
   * @return string[]
   *   The name of the extended class, the implemented interfaces and the traits
   *   used in the current class / interface / trait body.
   */
  public static function getExtendedImplemented(array $tokens): array {
    // Extended classes and implemented interfaces are between the class name
    // and the first curly brace.
    $name_control_index = static::getNameIndex($tokens);
    // class|trait|interface is followed by (at least a single) whitespace and
    // the the class name.
    $curly_brace_after_ns_index = static::getFirstIndexAfter($tokens, $name_control_index + 3, '{');
    $tokens_between_ns_curly = static::getTokensBetween($tokens, $name_control_index + 3, $curly_brace_after_ns_index);
    $extended_implemented = array_reduce(
      $tokens_between_ns_curly,
      function (array $carry, $item) {
        if ($item[0] === T_STRING) {
          $carry[] = $item[1];
        }
        return $carry;
      },
      []
    );

    $used_traits = self::getClassNamesFromUseStatements($tokens, TRUE);

    return array_merge($extended_implemented, $used_traits);
  }

  /**
   * Returns the namespace.
   *
   * @param array $tokens
   *   The tokens to search in.
   *
   * @return string
   *   The actual namespace, or NULL if it cannot be found.
   */
  public static function getNameSpace(array $tokens): ?string {
    $ns_index = self::getTokenIndex($tokens, T_NAMESPACE);
    if ($ns_index !== NULL) {
      return $tokens[$ns_index + 2][1] ?? NULL;
    }

    return NULL;
  }

  /**
   * Returns tokens between the declared tokens.
   *
   * @param array $tokens
   *   The tokens to search in.
   * @param int $start_index
   *   The index of the first token (it is excluded).
   * @param int|string|null $end_index
   *   The index of the last token (included). If integer is given, then it is
   *   evaluated as the key of the last token. If string is given, then the
   *   method tries to find the first token after the given starting point whose
   *   value is exactly the given string. If ommitted or is NULL, then every
   *   token is returned after the given starting point.
   *
   * @return array
   *   The tokens between the given start and end.
   *
   * @throws \LogicException
   */
  public static function getTokensBetween(array $tokens, int $start_index, $end_index = NULL): array {
    $start_offset = array_search($start_index, array_keys($tokens), TRUE);
    if ($start_offset === FALSE) {
      throw new \LogicException('Cannot find the given starting point.');
    }
    $split = array_slice($tokens, $start_offset, NULL, TRUE);
    if ($end_index === NULL) {
      return $split;
    }
    $end_index = is_string($end_index)
      ? array_search($end_index, $split, TRUE)
      : $end_index;

    if ($end_index === FALSE) {
      throw new \LogicException('Cannot find the given end point.');
    }

    $length = array_search($end_index, array_keys($split), TRUE);
    return array_slice($split, 0, $length, TRUE);
  }

  /**
   * Returns the actual class / trait / interface name.
   *
   * @param array $tokens
   *   The tokens to search in.
   *
   * @return string
   *   The actual class / trait / interface name.
   */
  public static function getName(array $tokens): ?string {
    $name_control_index = static::getNameIndex($tokens);
    return $tokens[$name_control_index + 2][1];
  }

  /**
   * Returns the index of the "quantifier" - 'class', 'trait' or 'interface'.
   *
   * The name of the class / trait / interface follows this word; they're
   * separated by a T_WHITESPACE token.
   *
   * @param array $tokens
   *   The tokens to search in.
   * @param int $type_token
   *   The T_CLASS, T_TRAIT or T_INTERFACE token. If ommitted, the every of them
   *   is tried.
   *
   * @return int|null
   *   The index of the first 'class', 'trait' or 'interface' token, or NULL
   *   if they aren't present.
   */
  public static function getNameIndex(array $tokens, int $type_token = NULL): ?int {
    $type_token = $type_token ?? self::NAME_TOKENS;
    foreach ($tokens as $k => $token) {
      if (
        in_array($token[0], (array) $type_token, TRUE) &&
        ($tokens[$k - 1][0] ?? NULL) !== T_DOUBLE_COLON
      ) {
        return $k;
      }
    }
    return NULL;
  }

  /**
   * Returns the first occurrence of the given token type or string.
   *
   * @param array $tokens
   *   The tokens to search in.
   * @param int|string $token_or_string
   *   The token name constant (an integer), or the string to search for.
   *
   * @return int|null
   *   The key of the first occurrence of the given token type or string, or
   *   NULL if no such token can be found.
   */
  public static function getTokenIndex(array $tokens, $token_or_string): ?int {
    if (is_int($token_or_string)) {
      foreach ($tokens as $index => $token) {
        if (!is_array($token) || $token[0] !== $token_or_string) {
          continue;
        }

        return $index;
      }
    }
    else {
      foreach ($tokens as $index => $token) {
        if (is_array($token) && $token[1] !== $token_or_string) {
          continue;
        }
        elseif (is_string($token) && $token !== $token_or_string) {
          continue;
        }

        return $index;
      }
    }

    return NULL;
  }

  /**
   * Builds a class file content from the given tokens.
   *
   * @param array $tokens
   *   The tokens of a class file.
   *
   * @return string
   *   A class file content as string.
   */
  public static function tokensToString(array $tokens): string {
    $map = array_map(
      function ($token) {
        return is_string($token) ? $token : $token[1];
      },
      $tokens
    );
    return implode('', $map);
  }

  /**
   * Returns the index of "abstract" or "final" in front of the given key.
   *
   * @param array $tokens
   *   The tokens of a class file.
   * @param int $key
   *   Index of a key.
   *
   * @return int|null
   *   The index of "abstract" or "final" in front of the given key, or NULL if
   *   it cannot be found.
   */
  public static function getKeyWordIndexBefore(array $tokens, int $key): ?int {
    return $key > 1 && in_array($tokens[$key - 2][0], [T_ABSTRACT, T_FINAL], TRUE)
      ? $key - 2
        : NULL;
  }

}
