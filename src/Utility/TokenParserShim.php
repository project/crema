<?php

declare(strict_types = 1);

namespace Drupal\crema\Utility;

/**
 * Shim for eliminating tokenizer differences between PHP versions.
 */
class TokenParserShim {

  /**
   * Eliminates tokenizer differences between PHP versions.
   *
   * @param array $tokens
   *   The tokens returned by token_get_all().
   *
   * @return array
   *   The "standardized" tokens.
   */
  public static function massageTokens(array $tokens): array {
    if (PHP_MAJOR_VERSION >= 8 && PHP_MINOR_VERSION >= 1) {
      return $tokens;
    }

    // T_STRING and T_NS_SEPARATOR tokens should be joined to a T_STRING
    // (this is how it works in PHP 8.1).
    $tokens_massaged = [];
    foreach ($tokens as $key => $token) {
      if (is_string($key)) {
        $tokens_massaged[$key] = self::massageTokens($token);
        continue;
      }

      $last_token = end($tokens_massaged) ?: NULL;

      if (
        ($last_token[0] ?? NULL) === T_STRING &&
        ($token[0] ?? NULL) === T_NS_SEPARATOR
      ) {
        $tokens_massaged[key($tokens_massaged)][1] .= $token[1];
      }
      elseif (
        ($last_token[0] ?? NULL) === T_STRING &&
        ($token[0] ?? NULL) === T_STRING
      ) {
        $tokens_massaged[key($tokens_massaged)][1] .= $token[1];
      }
      else {
        $tokens_massaged[] = $token;
      }
    }

    return $tokens_massaged;
  }

}
