<?php

namespace Crema\migrate\Plugin;

use Drupal\migrate\Plugin\MigrationPluginManager as Original;

/**
 * Replaces original.
 */
class MigrationPluginManager extends Original {

  /**
   * Foo.
   *
   * @return string
   *   A 'foo'.
   */
  public function foo(): string {
    return 'foo';
  }

}
