<?php

namespace Crema\migrate_drupal;

use Drupal\migrate_drupal\MigrationPluginManager as Original;

/**
 * Replaces original.
 */
class MigrationPluginManager extends Original {

  /**
   * Bar.
   *
   * @return string
   *   A 'bar'.
   */
  public function bar(): string {
    return 'bar';
  }

}
