<?php

namespace Crema\user\Entity;

use Drupal\user\Entity\User as OriginalUser;

/**
 * Class override.
 */
class User extends OriginalUser {

  /**
   * {@inheritdoc}
   */
  public function forgetInitialEmail(): void {
    $this->set('init', $this->getEmail());
  }

}
