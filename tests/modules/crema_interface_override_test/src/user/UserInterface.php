<?php

namespace Crema\user;

use Drupal\user\UserInterface as OriginalUserInterface;

/**
 * Interface override.
 */
interface UserInterface extends OriginalUserInterface {

  /**
   * User forgets initial email.
   */
  public function forgetInitialEmail(): void;

}
