<?php

namespace Drupal\Tests\crema\Unit\Utility;

use Drupal\crema\Utility\ClassCamouflageTokenParser;
use Drupal\crema\Utility\TokenParserShim;
use Drupal\Tests\UnitTestCase;

/**
 * Tests The camouflage token parser utility..
 *
 * @coversDefaultClass \Drupal\crema\Utility\ClassCamouflageTokenParser;
 *
 * @group crema
 */
class ClassCamouflageTokenParserTest extends UnitTestCase {

  /**
   * A very basic Drupal module file.
   *
   * @const string.
   */
  const MODULE_FILE = <<<'EOL'
<?php

/**
 * @file
 * Some file comment.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function foo_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.foo':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module does nothing') . '</p>';
      return $output;
  }
}

EOL;

  /**
   * @covers ::getTokenIndex
   *
   * @dataProvider providerTestGetTokenIndex
   */
  public function testGetTokenIndex(string $tokens_file, $token_needle, ?int $expected_index): void {
    $tokens = TokenParserShim::massageTokens(token_get_all($tokens_file));
    $this->assertSame(
      $expected_index,
      ClassCamouflageTokenParser::getTokenIndex($tokens, $token_needle)
    );
  }

  /**
   * Data provider for ::testGetTokenIndex.
   *
   * @return array[]
   *   The test cases.
   */
  public function providerTestGetTokenIndex(): array {
    return [
      'Missing token' => [
        'file content' => self::MODULE_FILE,
        'needle' => T_NAMESPACE,
        'expected' => NULL,
      ],
      'T_USE token' => [
        'file content' => self::MODULE_FILE,
        'needle' => T_USE,
        'expected' => 4,
      ],
      'T_FUNCTION token' => [
        'file content' => self::MODULE_FILE,
        'needle' => T_FUNCTION,
        'expected' => 11,
      ],
      'Open curly brace' => [
        'file content' => self::MODULE_FILE,
        'needle' => '{',
        'expected' => 23,
      ],
      'Semicolon' => [
        'file content' => self::MODULE_FILE,
        'needle' => ';',
        'expected' => 7,
      ],
      'T_SWITCH token' => [
        'file content' => self::MODULE_FILE,
        'needle' => T_SWITCH,
        'expected' => 25,
      ],
      'T_CASE token' => [
        'file content' => self::MODULE_FILE,
        'needle' => T_CASE,
        'expected' => 33,
      ],
      '$output variable (as string)' => [
        'file content' => self::MODULE_FILE,
        'needle' => '$output',
        'expected' => 38,
      ],
      "'return' as string" => [
        'file content' => self::MODULE_FILE,
        'needle' => 'return',
        'expected' => 81,
      ],
      "T_RETURN as token" => [
        'file content' => self::MODULE_FILE,
        'needle' => T_RETURN,
        'expected' => 81,
      ],
    ];
  }

  /**
   * Tests exception thrown in getSimplifiedExceptions.
   *
   * @covers ::getSimplifiedTokens
   *
   * @dataProvider providerGetSimplifiedExceptions
   */
  public function testGetSimplifiedExceptions(string $file_content, string $exception_message): void {
    $this->expectException(\LogicException::class);
    $this->expectExceptionMessage($exception_message);
    ClassCamouflageTokenParser::getSimplifiedTokens($file_content);
  }

  /**
   * Data provider for ::testGetSimplifiedExceptions.
   *
   * @return string[][]
   *   The test cases.
   */
  public function providerGetSimplifiedExceptions(): array {
    return [
      'no open tag' => [
        'file' => "A text file\nwith some line breaks...",
        'exception message' => 'The tokenized PHP class must begin with a PHP open tag.',
      ],
      'open tag not the first' => [
        'file' => "Some test then\n<?php",
        'exception message' => 'The tokenized PHP class must begin with a PHP open tag.',
      ],
      'no namespace' => [
        'file' => self::MODULE_FILE,
        'exception message' => 'The tokenized PHP class must have a namespace.',
      ],
      'no class name' => [
        'file' => "<?php\nnamespace Foo;\nfunction do_nothing() {};\n",
        'exception message' => 'The tokenized PHP class does not contain any class name.',
      ],
    ];
  }

}
