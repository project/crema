<?php

namespace Drupal\Tests\crema\Unit;

use Drupal\crema\ClassCamouflage;
use Drupal\Tests\UnitTestCase;

/**
 * Tests ClassCamouflage.
 *
 * @coversDefaultClass \Drupal\crema\ClassCamouflage;
 *
 * @group crema
 */
class ClassCamouflageTest extends UnitTestCase {

  /**
   * Tests camouflage classes.
   *
   * @dataProvider camouflagedProvider
   */
  public function testGetCamouflaged(string $file_content, string $expected): void {
    $classCamouflage = new ClassCamouflage($file_content);
    $this->assertSame(
      $expected,
      $classCamouflage->getCamouflaged()
    );
  }

  /**
   * Data provider for ::testGetCamouflaged.
   *
   * @return string[][]
   *   The test cases.
   */
  public function camouflagedProvider(): array {
    return [
      'Simple class' => CremaTestClasses::SIMPLE_CLASS,
      'A bit more complicated trait' => CremaTestClasses::SIMPLE_TRAIT,
      'A complex class' => CremaTestClasses::COMPLEX_CLASS,
      'A very complex class' => CremaTestClasses::VERY_COMPLEX_CLASS,
      'User class' => CremaUserTestClasses::USER_CLASS,
      'User interface' => CremaUserTestClasses::USER_INTERFACE,
    ];
  }

  /**
   * Tests new classes.
   *
   * @dataProvider newProvider
   */
  public function testGetNew(string $file_content, string $expected): void {
    $classCamouflage = new ClassCamouflage($file_content);
    $this->assertSame(
      $expected,
      $classCamouflage->getNew()
    );
  }

  /**
   * Data provider for ::testGetNew.
   *
   * @return string[][]
   *   The test cases.
   */
  public function newProvider(): array {
    return [
      'Simple class' => [
        'class' => <<<'EOL'
<?php

namespace Crema\migrate\Plugin;

use Drupal\migrate\Plugin\MigrationPluginManager as Original;

/**
 * Plugin manager for migration plugins.
 */
class MigrationPluginManager extends Original {

  /**
   * @return string
   */
  public function foo(): string {
    return 'foo';
  }

}
EOL
        ,
        'expected' => <<<'EOL'
<?php

namespace Drupal\migrate\Plugin;

use Drupal\_original_\migrate\Plugin\MigrationPluginManager as Original;

class MigrationPluginManager extends Original {

  public function foo(): string {
    return 'foo';
  }

}
EOL
        ,
      ],
    ];
  }

}
