<?php

namespace Drupal\Tests\crema\Unit;

/**
 * Contains test classes and their expected camouflage.
 */
class CremaUserTestClasses {

  /**
   * The slightly modified User class form core, and its expected camouflage.
   *
   * @see \Drupal\user\Entity\User
   *
   * @const string[]
   */
  const USER_CLASS = [
    'original' => <<<'EOL'
<?php

namespace Drupal\user\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Language\LanguageInterface;
use Drupal\user\RoleInterface;
use Drupal\user\StatusItem;
use Drupal\user\TimeZoneItem;
use Drupal\user\UserInterface;

/**
 * Defines the user entity class.
 *
 * The base table name here is plural, despite Drupal table naming standards,
 * because "user" is a reserved word in many databases.
 *
 * @ContentEntityType(
 *   id = "user",
 *   label = @Translation("User"),
 *   label_collection = @Translation("Users"),
 *   label_singular = @Translation("user"),
 *   label_plural = @Translation("users"),
 *   label_count = @PluralTranslation(
 *     singular = "@count user",
 *     plural = "@count users",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\user\UserStorage",
 *     "storage_schema" = "Drupal\user\UserStorageSchema",
 *     "access" = "Drupal\user\UserAccessControlHandler",
 *     "list_builder" = "Drupal\user\UserListBuilder",
 *     "views_data" = "Drupal\user\UserViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\user\Entity\UserRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\user\ProfileForm",
 *       "cancel" = "Drupal\user\Form\UserCancelForm",
 *       "register" = "Drupal\user\RegisterForm"
 *     },
 *     "translation" = "Drupal\user\ProfileTranslationHandler"
 *   },
 *   admin_permission = "administer users",
 *   base_table = "users",
 *   data_table = "users_field_data",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "uid",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/user/{user}",
 *     "edit-form" = "/user/{user}/edit",
 *     "cancel-form" = "/user/{user}/cancel",
 *     "collection" = "/admin/people",
 *   },
 *   field_ui_base_route = "entity.user.admin_form",
 *   common_reference_target = TRUE
 * )
 */
class User extends ContentEntityBase implements UserInterface {

  use EntityChangedTrait;
  protected static $anonymousUser;
  public function isNew() {
    return !empty($this->enforceIsNew) || $this->id() === NULL;
  }
  public function label() {
    return $this->getDisplayName();
  }
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // Make sure that the authenticated/anonymous roles are not persisted.
    foreach ($this->get('roles') as $index => $item) {
      if (in_array($item->target_id, [RoleInterface::ANONYMOUS_ID, RoleInterface::AUTHENTICATED_ID])) {
        $this->get('roles')->offsetUnset($index);
      }
    }

    // Store account cancellation information.
    foreach (['user_cancel_method', 'user_cancel_notify'] as $key) {
      if (isset($this->{$key})) {
        \Drupal::service('user.data')->set('user', $this->id(), substr($key, 5), $this->{$key});
      }
    }
  }
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    if ($update) {
      $session_manager = \Drupal::service('session_manager');
      // If the password has been changed, delete all open sessions for the
      // user and recreate the current one.
      if ($this->pass->value != $this->original->pass->value) {
        $session_manager->delete($this->id());
        if ($this->id() == \Drupal::currentUser()->id()) {
          \Drupal::service('session')->migrate();
        }
      }

      // If the user was blocked, delete the user's sessions to force a logout.
      if ($this->original->status->value != $this->status->value && $this->status->value == 0) {
        $session_manager->delete($this->id());
      }

      // Send emails after we have the new user object.
      if ($this->status->value != $this->original->status->value) {
        // The user's status is changing; conditionally send notification email.
        $op = $this->status->value == 1 ? 'status_activated' : 'status_blocked';
        _user_mail_notify($op, $this);
      }
    }
  }
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    $uids = array_keys($entities);
    \Drupal::service('user.data')->delete(NULL, $uids);
  }
  public function getRoles($exclude_locked_roles = FALSE) {
    $roles = [];

    // Users with an ID always have the authenticated user role.
    if (!$exclude_locked_roles) {
      if ($this->isAuthenticated()) {
        $roles[] = RoleInterface::AUTHENTICATED_ID;
      }
      else {
        $roles[] = RoleInterface::ANONYMOUS_ID;
      }
    }

    foreach ($this->get('roles') as $role) {
      if ($role->target_id) {
        $roles[] = $role->target_id;
      }
    }

    return $roles;
  }
  public function hasRole($rid) {
    return in_array($rid, $this->getRoles());
  }
  public function addRole($rid) {

    if (in_array($rid, [RoleInterface::AUTHENTICATED_ID, RoleInterface::ANONYMOUS_ID])) {
      throw new \InvalidArgumentException('Anonymous or authenticated role ID must not be assigned manually.');
    }

    $roles = $this->getRoles(TRUE);
    $roles[] = $rid;
    $this->set('roles', array_unique($roles));
  }
  public function removeRole($rid) {
    $this->set('roles', array_diff($this->getRoles(TRUE), [$rid]));
  }
  public function hasPermission($permission) {
    // User #1 has all privileges.
    if ((int) $this->id() === 1) {
      return TRUE;
    }

    return $this->getRoleStorage()->isPermissionInRoles($permission, $this->getRoles());
  }
  public function getPassword() {
    return $this->get('pass')->value;
  }
  public function setPassword($password) {
    $this->get('pass')->value = $password;
    return $this;
  }
  public function getEmail() {
    return $this->get('mail')->value;
  }
  public function setEmail($mail) {
    $this->get('mail')->value = $mail;
    return $this;
  }
  public function getCreatedTime() {
    return $this->get('created')->value;
  }
  public function getLastAccessedTime() {
    return $this->get('access')->value;
  }
  public function setLastAccessTime($timestamp) {
    $this->get('access')->value = $timestamp;
    return $this;
  }
  public function getLastLoginTime() {
    return $this->get('login')->value;
  }
  public function setLastLoginTime($timestamp) {
    $this->get('login')->value = $timestamp;
    return $this;
  }
  public function isActive() {
    return $this->get('status')->value == 1;
  }
  public function isBlocked() {
    return $this->get('status')->value == 0;
  }
  public function activate() {
    $this->get('status')->value = 1;
    return $this;
  }
  public function block() {
    $this->get('status')->value = 0;
    return $this;
  }
  public function getTimeZone() {
    return $this->get('timezone')->value;
  }
  public function getPreferredLangcode($fallback_to_default = TRUE) {
    $language_list = $this->languageManager()->getLanguages();
    $preferred_langcode = $this->get('preferred_langcode')->value;
    if (!empty($preferred_langcode) && isset($language_list[$preferred_langcode])) {
      return $language_list[$preferred_langcode]->getId();
    }
    else {
      return $fallback_to_default ? $this->languageManager()->getDefaultLanguage()->getId() : '';
    }
  }
  public function getPreferredAdminLangcode($fallback_to_default = TRUE) {
    $language_list = $this->languageManager()->getLanguages();
    $preferred_langcode = $this->get('preferred_admin_langcode')->value;
    if (!empty($preferred_langcode) && isset($language_list[$preferred_langcode])) {
      return $language_list[$preferred_langcode]->getId();
    }
    else {
      return $fallback_to_default ? $this->languageManager()->getDefaultLanguage()->getId() : '';
    }
  }
  public function getInitialEmail() {
    return $this->get('init')->value;
  }
  public function isAuthenticated() {
    return $this->id() > 0;
  }
  public function isAnonymous() {
    return $this->id() == 0;
  }
  public function getAccountName() {
    return $this->get('name')->value ?: '';
  }
  public function getDisplayName() {
    $name = $this->getAccountName() ?: \Drupal::config('user.settings')->get('anonymous');
    \Drupal::moduleHandler()->alter('user_format_name', $name, $this);
    return $name;
  }
  public function setUsername($username) {
    $this->set('name', $username);
    return $this;
  }
  public function setExistingPassword($password) {
    $this->get('pass')->existing = $password;
  }
  public function checkExistingPassword(UserInterface $account_unchanged) {
    $existing = $this->get('pass')->existing;
    return $existing !== NULL && strlen($existing) > 0 &&
      \Drupal::service('password')->check(trim($existing), $account_unchanged->getPassword());
  }
  public static function getAnonymousUser() {
    if (!isset(static::$anonymousUser)) {

      // @todo Use the entity factory once available, see
      //   https://www.drupal.org/node/1867228.
      $entity_type_manager = \Drupal::entityTypeManager();
      $entity_type = $entity_type_manager->getDefinition('user');
      $class = $entity_type->getClass();

      static::$anonymousUser = new $class([
        'uid' => [LanguageInterface::LANGCODE_DEFAULT => 0],
        'name' => [LanguageInterface::LANGCODE_DEFAULT => ''],
        // Explicitly set the langcode to ensure that field definitions do not
        // need to be fetched to figure out a default.
        'langcode' => [LanguageInterface::LANGCODE_DEFAULT => LanguageInterface::LANGCODE_NOT_SPECIFIED],
      ], $entity_type->id());
    }
    return clone static::$anonymousUser;
  }
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid']->setLabel(t('User ID'))
      ->setDescription(t('The user ID.'));

    $fields['uuid']->setDescription(t('The user UUID.'));

    $fields['langcode']->setLabel(t('Language code'))
      ->setDescription(t('The user language code.'))
      ->setDisplayOptions('form', ['region' => 'hidden']);

    $fields['preferred_langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Preferred language code'))
      ->setDescription(t("The user's preferred language code for receiving emails and viewing the site."))
      // @todo: Define this via an options provider once
      // https://www.drupal.org/node/2329937 is completed.
      ->addPropertyConstraints('value', [
        'AllowedValues' => ['callback' => __CLASS__ . '::getAllowedConfigurableLanguageCodes'],
      ]);

    $fields['preferred_admin_langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Preferred admin language code'))
      ->setDescription(t("The user's preferred language code for viewing administration pages."))
      // @todo: A default value of NULL is ignored, so we have to specify
      // an empty field item structure instead. Fix this in
      // https://www.drupal.org/node/2318605.
      ->setDefaultValue([0 => ['value' => NULL]])
      // @todo: Define this via an options provider once
      // https://www.drupal.org/node/2329937 is completed.
      ->addPropertyConstraints('value', [
        'AllowedValues' => ['callback' => __CLASS__ . '::getAllowedConfigurableLanguageCodes'],
      ]);

    // The name should not vary per language. The username is the visual
    // identifier for a user and needs to be consistent in all languages.
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of this user.'))
      ->setRequired(TRUE)
      ->setConstraints([
        // No Length constraint here because the UserName constraint also covers
        // that.
        'UserName' => [],
        'UserNameUnique' => [],
      ]);
    $fields['name']->getItemDefinition()->setClass('\Drupal\user\UserNameItem');

    $fields['pass'] = BaseFieldDefinition::create('password')
      ->setLabel(t('Password'))
      ->setDescription(t('The password of this user (hashed).'))
      ->addConstraint('ProtectedUserField');

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setDescription(t('The email of this user.'))
      ->setDefaultValue('')
      ->addConstraint('UserMailUnique')
      ->addConstraint('UserMailRequired')
      ->addConstraint('ProtectedUserField');

    $fields['timezone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Timezone'))
      ->setDescription(t('The timezone of this user.'))
      ->setSetting('max_length', 32)
      // @todo: Define this via an options provider once
      // https://www.drupal.org/node/2329937 is completed.
      ->addPropertyConstraints('value', [
        'AllowedValues' => ['callback' => __CLASS__ . '::getAllowedTimezones'],
      ]);
    $fields['timezone']->getItemDefinition()->setClass(TimeZoneItem::class);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('User status'))
      ->setDescription(t('Whether the user is active or blocked.'))
      ->setDefaultValue(FALSE);
    $fields['status']->getItemDefinition()->setClass(StatusItem::class);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the user was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the user was last edited.'))
      ->setTranslatable(TRUE);

    $fields['access'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last access'))
      ->setDescription(t('The time that the user last accessed the site.'))
      ->setDefaultValue(0);

    $fields['login'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last login'))
      ->setDescription(t('The time that the user last logged in.'))
      ->setDefaultValue(0);

    $fields['init'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Initial email'))
      ->setDescription(t('The email address used for initial account creation.'))
      ->setDefaultValue('');

    $fields['roles'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Roles'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDescription(t('The roles the user has.'))
      ->setSetting('target_type', 'user_role');

    return $fields;
  }
  protected function getRoleStorage() {
    return \Drupal::entityTypeManager()->getStorage('user_role');
  }
  public static function getAllowedTimezones() {
    return array_keys(system_time_zones());
  }
  public static function getAllowedConfigurableLanguageCodes() {
    return array_keys(\Drupal::languageManager()->getLanguages(LanguageInterface::STATE_CONFIGURABLE));
  }

}
EOL
    ,
    'expected' => <<<'EOL'
<?php

namespace Drupal\_original_\user\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Language\LanguageInterface;
use Drupal\user\RoleInterface;
use Drupal\user\StatusItem;
use Drupal\user\TimeZoneItem;
use Drupal\user\UserInterface;

abstract class User extends ContentEntityBase implements UserInterface {

  use EntityChangedTrait;
  protected static $anonymousUser;
  public function isNew() {
    return !empty($this->enforceIsNew) || $this->id() === NULL;
  }
  public function label() {
    return $this->getDisplayName();
  }
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach ($this->get('roles') as $index => $item) {
      if (in_array($item->target_id, [RoleInterface::ANONYMOUS_ID, RoleInterface::AUTHENTICATED_ID])) {
        $this->get('roles')->offsetUnset($index);
      }
    }

    foreach (['user_cancel_method', 'user_cancel_notify'] as $key) {
      if (isset($this->{$key})) {
        \Drupal::service('user.data')->set('user', $this->id(), substr($key, 5), $this->{$key});
      }
    }
  }
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    if ($update) {
      $session_manager = \Drupal::service('session_manager');
      if ($this->pass->value != $this->original->pass->value) {
        $session_manager->delete($this->id());
        if ($this->id() == \Drupal::currentUser()->id()) {
          \Drupal::service('session')->migrate();
        }
      }

      if ($this->original->status->value != $this->status->value && $this->status->value == 0) {
        $session_manager->delete($this->id());
      }

      if ($this->status->value != $this->original->status->value) {
        $op = $this->status->value == 1 ? 'status_activated' : 'status_blocked';
        _user_mail_notify($op, $this);
      }
    }
  }
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    $uids = array_keys($entities);
    \Drupal::service('user.data')->delete(NULL, $uids);
  }
  public function getRoles($exclude_locked_roles = FALSE) {
    $roles = [];

    if (!$exclude_locked_roles) {
      if ($this->isAuthenticated()) {
        $roles[] = RoleInterface::AUTHENTICATED_ID;
      }
      else {
        $roles[] = RoleInterface::ANONYMOUS_ID;
      }
    }

    foreach ($this->get('roles') as $role) {
      if ($role->target_id) {
        $roles[] = $role->target_id;
      }
    }

    return $roles;
  }
  public function hasRole($rid) {
    return in_array($rid, $this->getRoles());
  }
  public function addRole($rid) {

    if (in_array($rid, [RoleInterface::AUTHENTICATED_ID, RoleInterface::ANONYMOUS_ID])) {
      throw new \InvalidArgumentException('Anonymous or authenticated role ID must not be assigned manually.');
    }

    $roles = $this->getRoles(TRUE);
    $roles[] = $rid;
    $this->set('roles', array_unique($roles));
  }
  public function removeRole($rid) {
    $this->set('roles', array_diff($this->getRoles(TRUE), [$rid]));
  }
  public function hasPermission($permission) {
    if ((int) $this->id() === 1) {
      return TRUE;
    }

    return $this->getRoleStorage()->isPermissionInRoles($permission, $this->getRoles());
  }
  public function getPassword() {
    return $this->get('pass')->value;
  }
  public function setPassword($password) {
    $this->get('pass')->value = $password;
    return $this;
  }
  public function getEmail() {
    return $this->get('mail')->value;
  }
  public function setEmail($mail) {
    $this->get('mail')->value = $mail;
    return $this;
  }
  public function getCreatedTime() {
    return $this->get('created')->value;
  }
  public function getLastAccessedTime() {
    return $this->get('access')->value;
  }
  public function setLastAccessTime($timestamp) {
    $this->get('access')->value = $timestamp;
    return $this;
  }
  public function getLastLoginTime() {
    return $this->get('login')->value;
  }
  public function setLastLoginTime($timestamp) {
    $this->get('login')->value = $timestamp;
    return $this;
  }
  public function isActive() {
    return $this->get('status')->value == 1;
  }
  public function isBlocked() {
    return $this->get('status')->value == 0;
  }
  public function activate() {
    $this->get('status')->value = 1;
    return $this;
  }
  public function block() {
    $this->get('status')->value = 0;
    return $this;
  }
  public function getTimeZone() {
    return $this->get('timezone')->value;
  }
  public function getPreferredLangcode($fallback_to_default = TRUE) {
    $language_list = $this->languageManager()->getLanguages();
    $preferred_langcode = $this->get('preferred_langcode')->value;
    if (!empty($preferred_langcode) && isset($language_list[$preferred_langcode])) {
      return $language_list[$preferred_langcode]->getId();
    }
    else {
      return $fallback_to_default ? $this->languageManager()->getDefaultLanguage()->getId() : '';
    }
  }
  public function getPreferredAdminLangcode($fallback_to_default = TRUE) {
    $language_list = $this->languageManager()->getLanguages();
    $preferred_langcode = $this->get('preferred_admin_langcode')->value;
    if (!empty($preferred_langcode) && isset($language_list[$preferred_langcode])) {
      return $language_list[$preferred_langcode]->getId();
    }
    else {
      return $fallback_to_default ? $this->languageManager()->getDefaultLanguage()->getId() : '';
    }
  }
  public function getInitialEmail() {
    return $this->get('init')->value;
  }
  public function isAuthenticated() {
    return $this->id() > 0;
  }
  public function isAnonymous() {
    return $this->id() == 0;
  }
  public function getAccountName() {
    return $this->get('name')->value ?: '';
  }
  public function getDisplayName() {
    $name = $this->getAccountName() ?: \Drupal::config('user.settings')->get('anonymous');
    \Drupal::moduleHandler()->alter('user_format_name', $name, $this);
    return $name;
  }
  public function setUsername($username) {
    $this->set('name', $username);
    return $this;
  }
  public function setExistingPassword($password) {
    $this->get('pass')->existing = $password;
  }
  public function checkExistingPassword(UserInterface $account_unchanged) {
    $existing = $this->get('pass')->existing;
    return $existing !== NULL && strlen($existing) > 0 &&
      \Drupal::service('password')->check(trim($existing), $account_unchanged->getPassword());
  }
  public static function getAnonymousUser() {
    if (!isset(static::$anonymousUser)) {

      $entity_type_manager = \Drupal::entityTypeManager();
      $entity_type = $entity_type_manager->getDefinition('user');
      $class = $entity_type->getClass();

      static::$anonymousUser = new $class([
        'uid' => [LanguageInterface::LANGCODE_DEFAULT => 0],
        'name' => [LanguageInterface::LANGCODE_DEFAULT => ''],
        'langcode' => [LanguageInterface::LANGCODE_DEFAULT => LanguageInterface::LANGCODE_NOT_SPECIFIED],
      ], $entity_type->id());
    }
    return clone static::$anonymousUser;
  }
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid']->setLabel(t('User ID'))
      ->setDescription(t('The user ID.'));

    $fields['uuid']->setDescription(t('The user UUID.'));

    $fields['langcode']->setLabel(t('Language code'))
      ->setDescription(t('The user language code.'))
      ->setDisplayOptions('form', ['region' => 'hidden']);

    $fields['preferred_langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Preferred language code'))
      ->setDescription(t("The user's preferred language code for receiving emails and viewing the site."))
      ->addPropertyConstraints('value', [
        'AllowedValues' => ['callback' => __CLASS__ . '::getAllowedConfigurableLanguageCodes'],
      ]);

    $fields['preferred_admin_langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Preferred admin language code'))
      ->setDescription(t("The user's preferred language code for viewing administration pages."))
      ->setDefaultValue([0 => ['value' => NULL]])
      ->addPropertyConstraints('value', [
        'AllowedValues' => ['callback' => __CLASS__ . '::getAllowedConfigurableLanguageCodes'],
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of this user.'))
      ->setRequired(TRUE)
      ->setConstraints([
        'UserName' => [],
        'UserNameUnique' => [],
      ]);
    $fields['name']->getItemDefinition()->setClass('\Drupal\user\UserNameItem');

    $fields['pass'] = BaseFieldDefinition::create('password')
      ->setLabel(t('Password'))
      ->setDescription(t('The password of this user (hashed).'))
      ->addConstraint('ProtectedUserField');

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setDescription(t('The email of this user.'))
      ->setDefaultValue('')
      ->addConstraint('UserMailUnique')
      ->addConstraint('UserMailRequired')
      ->addConstraint('ProtectedUserField');

    $fields['timezone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Timezone'))
      ->setDescription(t('The timezone of this user.'))
      ->setSetting('max_length', 32)
      ->addPropertyConstraints('value', [
        'AllowedValues' => ['callback' => __CLASS__ . '::getAllowedTimezones'],
      ]);
    $fields['timezone']->getItemDefinition()->setClass(TimeZoneItem::class);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('User status'))
      ->setDescription(t('Whether the user is active or blocked.'))
      ->setDefaultValue(FALSE);
    $fields['status']->getItemDefinition()->setClass(StatusItem::class);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the user was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the user was last edited.'))
      ->setTranslatable(TRUE);

    $fields['access'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last access'))
      ->setDescription(t('The time that the user last accessed the site.'))
      ->setDefaultValue(0);

    $fields['login'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last login'))
      ->setDescription(t('The time that the user last logged in.'))
      ->setDefaultValue(0);

    $fields['init'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Initial email'))
      ->setDescription(t('The email address used for initial account creation.'))
      ->setDefaultValue('');

    $fields['roles'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Roles'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDescription(t('The roles the user has.'))
      ->setSetting('target_type', 'user_role');

    return $fields;
  }
  protected function getRoleStorage() {
    return \Drupal::entityTypeManager()->getStorage('user_role');
  }
  public static function getAllowedTimezones() {
    return array_keys(system_time_zones());
  }
  public static function getAllowedConfigurableLanguageCodes() {
    return array_keys(\Drupal::languageManager()->getLanguages(LanguageInterface::STATE_CONFIGURABLE));
  }

}
EOL
    ,
  ];

  /**
   * The UserInterface class form core, and its expected camouflage.
   *
   * @see \Drupal\user\UserInterface
   *
   * @const string[]
   */
  const USER_INTERFACE = [
    'original' => <<<'EOL'
<?php

namespace Drupal\user;

use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an interface defining a user entity.
 *
 * @ingroup user_api
 */
interface UserInterface extends ContentEntityInterface, EntityChangedInterface, AccountInterface {

  /**
   * Maximum length of username text field.
   *
   * Keep this under 191 characters so we can use a unique constraint in MySQL.
   */
  const USERNAME_MAX_LENGTH = 60;

  /**
   * Only administrators can create user accounts.
   */
  const REGISTER_ADMINISTRATORS_ONLY = 'admin_only';

  /**
   * Visitors can create their own accounts.
   */
  const REGISTER_VISITORS = 'visitors';

  /**
   * Visitors can create accounts that only become active with admin approval.
   */
  const REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL = 'visitors_admin_approval';

  /**
   * New users will be set to the default time zone at registration.
   */
  const TIMEZONE_DEFAULT = 0;

  /**
   * New users will get an empty time zone at registration.
   */
  const TIMEZONE_EMPTY = 1;

  /**
   * New users will select their own timezone at registration.
   */
  const TIMEZONE_SELECT = 2;

  /**
   * Whether a user has a certain role.
   *
   * @param string $rid
   *   The role ID to check.
   *
   * @return bool
   *   Returns TRUE if the user has the role, otherwise FALSE.
   */
  public function hasRole($rid);

  /**
   * Add a role to a user.
   *
   * @param string $rid
   *   The role ID to add.
   */
  public function addRole($rid);

  /**
   * Remove a role from a user.
   *
   * @param string $rid
   *   The role ID to remove.
   */
  public function removeRole($rid);

  /**
   * Sets the username of this account.
   *
   * @param string $username
   *   The new user name.
   *
   * @return $this
   *   The called user entity.
   */
  public function setUsername($username);

  /**
   * Returns the hashed password.
   *
   * @return string
   *   The hashed password.
   */
  public function getPassword();

  /**
   * Sets the user password.
   *
   * @param string $password
   *   The new unhashed password.
   *
   * @return $this
   *   The called user entity.
   */
  public function setPassword($password);

  /**
   * Sets the email address of the user.
   *
   * @param string $mail
   *   The new email address of the user.
   *
   * @return $this
   *   The called user entity.
   */
  public function setEmail($mail);

  /**
   * Returns the creation time of the user as a UNIX timestamp.
   *
   * @return int
   *   Timestamp of the creation date.
   */
  public function getCreatedTime();

  /**
   * Sets the UNIX timestamp when the user last accessed the site..
   *
   * @param int $timestamp
   *   Timestamp of the last access.
   *
   * @return $this
   *   The called user entity.
   */
  public function setLastAccessTime($timestamp);

  /**
   * Returns the UNIX timestamp when the user last logged in.
   *
   * @return int
   *   Timestamp of the last login time.
   */
  public function getLastLoginTime();

  /**
   * Sets the UNIX timestamp when the user last logged in.
   *
   * @param int $timestamp
   *   Timestamp of the last login time.
   *
   * @return $this
   *   The called user entity.
   */
  public function setLastLoginTime($timestamp);

  /**
   * Returns TRUE if the user is active.
   *
   * @return bool
   *   TRUE if the user is active, false otherwise.
   */
  public function isActive();

  /**
   * Returns TRUE if the user is blocked.
   *
   * @return bool
   *   TRUE if the user is blocked, false otherwise.
   */
  public function isBlocked();

  /**
   * Activates the user.
   *
   * @return $this
   *   The called user entity.
   */
  public function activate();

  /**
   * Blocks the user.
   *
   * @return $this
   *   The called user entity.
   */
  public function block();

  /**
   * Returns the email that was used when the user was registered.
   *
   * @return string
   *   Initial email address of the user.
   */
  public function getInitialEmail();

  /**
   * Sets the existing plain text password.
   *
   * Required for validation when changing the password, name or email fields.
   *
   * @param string $password
   *   The existing plain text password of the user.
   *
   * @return $this
   */
  public function setExistingPassword($password);

  /**
   * Checks the existing password if set.
   *
   * @param \Drupal\user\UserInterface $account_unchanged
   *   The unchanged user entity to compare against.
   *
   * @return bool
   *   TRUE if the correct existing password was provided.
   *
   * @see UserInterface::setExistingPassword()
   */
  public function checkExistingPassword(UserInterface $account_unchanged);

}

EOL
    ,
    'expected' => <<<'EOL'
<?php

namespace Drupal\_original_\user;

use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;

interface UserInterface extends ContentEntityInterface, EntityChangedInterface, AccountInterface {

  const USERNAME_MAX_LENGTH = 60;

  const REGISTER_ADMINISTRATORS_ONLY = 'admin_only';

  const REGISTER_VISITORS = 'visitors';

  const REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL = 'visitors_admin_approval';

  const TIMEZONE_DEFAULT = 0;

  const TIMEZONE_EMPTY = 1;

  const TIMEZONE_SELECT = 2;

  public function hasRole($rid);

  public function addRole($rid);

  public function removeRole($rid);

  public function setUsername($username);

  public function getPassword();

  public function setPassword($password);

  public function setEmail($mail);

  public function getCreatedTime();

  public function setLastAccessTime($timestamp);

  public function getLastLoginTime();

  public function setLastLoginTime($timestamp);

  public function isActive();

  public function isBlocked();

  public function activate();

  public function block();

  public function getInitialEmail();

  public function setExistingPassword($password);

  public function checkExistingPassword(\Drupal\user\UserInterface $account_unchanged);

}

EOL
    ,
  ];

}
