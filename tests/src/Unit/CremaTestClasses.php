<?php

namespace Drupal\Tests\crema\Unit;

/**
 * Contains test classes and their expected camouflage.
 */
class CremaTestClasses {

  /**
   * A very simple abstract PHP class.
   *
   * @const string[]
   */
  const SIMPLE_CLASS = [
    'original' => <<<'EOL'
<?php

namespace Drupal\Foo;

abstract class FooBarBase extends FooBase implements FooInterface, BarInterface {

  use FooTrait;

}
EOL
    ,
    'expected' => <<<'EOL'
<?php

namespace Drupal\_original_\Foo;

use Drupal\Foo\FooBase;
use Drupal\Foo\FooInterface;
use Drupal\Foo\BarInterface;
use Drupal\Foo\FooTrait;

abstract class FooBarBase extends FooBase implements FooInterface, BarInterface {

  use FooTrait;

}
EOL
    ,
  ];

  /**
   * A simple PHP trait.
   *
   * @const string[]
   */
  const SIMPLE_TRAIT = [
    'class' => <<<'EOL'
<?php

namespace Drupal\foo\Bar;

use Drupal\baz\BazTrait as OriginalBaseTrait;
use Drupal\foo\BarInterface;
use Drupal\foo\FooBar as FooBarBaz;

/**
 * Test trait.
 *
 * @see \Drupal\foo\Bar\FooBarBaz
 */
trait BazTrait extends BazBaseTrait {

  use BazExtraLogicTrait{
    setup as public traitSetup;
  }
  use OriginalBaseTrait;

  public static function fooBarBaz(self $self, parent $parent, array $array, callable $callable, bool $bool, float $float, int $int, string $string, iterable $iterable, object $object, mixed $mixed, int|float|bool|null $union): bool {
    assert($object instanceof BarInterface);
    if (!$mixed instanceof ClassFromSameNs) {
      throw new ExceptionFromSameNs();
    }
    return FooBarBaz::isThisFine(func_get_args());
  }

  use FooFixtureTrait;

}

EOL
    ,
    'expected' => <<<'EOL'
<?php

namespace Drupal\_original_\foo\Bar;

use Drupal\foo\Bar\BazBaseTrait;
use Drupal\foo\Bar\BazExtraLogicTrait;
use Drupal\foo\Bar\FooFixtureTrait;
use Drupal\foo\Bar\ClassFromSameNs;
use Drupal\foo\Bar\ExceptionFromSameNs;
use Drupal\baz\BazTrait as OriginalBaseTrait;
use Drupal\foo\BarInterface;
use Drupal\foo\FooBar as FooBarBaz;

trait BazTrait extends BazBaseTrait {

  use BazExtraLogicTrait{
    setup as public traitSetup;
  }
  use OriginalBaseTrait;

  public static function fooBarBaz(self $self, parent $parent, array $array, callable $callable, bool $bool, float $float, int $int, string $string, iterable $iterable, object $object, mixed $mixed, int|float|bool|null $union): bool {
    assert($object instanceof BarInterface);
    if (!$mixed instanceof ClassFromSameNs) {
      throw new ExceptionFromSameNs();
    }
    return FooBarBaz::isThisFine(func_get_args());
  }

  use FooFixtureTrait;

}

EOL
    ,
  ];

  /**
   * A complex PHP trait.
   *
   * @const string[]
   */
  const COMPLEX_CLASS = [
    'class' => <<<'EOL'
<?php

namespace Drupal\foo\Bar;

use Drupal\foo\BarInterface;
use Drupal\foo\FooBar as FooBarAlias;

if (class_exists(FooBazInterface::class)) {
  class Baz extends FooBarAlias implements BarInterface, FooBazInterface, InterfaceInSameNs {

    use TraitInSameNs;

  }
}
else {
  class Baz extends FooBarAlias implements BarInterface, InterfaceInSameNs {

    use TraitInSameNs;
    use AnotherTraitInSameNs {
      foo as antotherTraitFoo;
    }
    use YetAntotherTrait;

  }
}
EOL
    ,
    'actual' => <<<'EOL'
<?php

namespace Drupal\_original_\foo\Bar;

use Drupal\foo\Bar\FooBazInterface;
use Drupal\foo\Bar\InterfaceInSameNs;
use Drupal\foo\Bar\TraitInSameNs;
use Drupal\foo\Bar\AnotherTraitInSameNs;
use Drupal\foo\Bar\YetAntotherTrait;
use Drupal\foo\BarInterface;
use Drupal\foo\FooBar as FooBarAlias;

if (class_exists(FooBazInterface::class)) {
  abstract class Baz extends FooBarAlias implements BarInterface, FooBazInterface, InterfaceInSameNs {

    use TraitInSameNs;

  }
}
else {
  abstract class Baz extends FooBarAlias implements BarInterface, InterfaceInSameNs {

    use TraitInSameNs;
    use AnotherTraitInSameNs {
      foo as antotherTraitFoo;
    }
    use YetAntotherTrait;

  }
}
EOL
    ,
  ];

  /**
   * A very ( :) ) complex class.
   *
   * @const string[]
   */
  const VERY_COMPLEX_CLASS = [
    'class' => <<<'EOL'
<?php

namespace Drupal\foo\Bar;

use Drupal\foo\BarInterface;
use Drupal\foo\FooBar as FooBarAlias;

if (class_exists(FooBazBarInterface::class)) {
  class Baz extends FooBarAlias implements BarInterface, FooBazInterface, InterfaceInSameNs {

    use TraitInSameNs;

    public function superFoo(array $foo, string $bar, AnotherUnusedClass $baz, YetAnother1|YetAnother2|iterable $union): array {
      return array_filter(
        $foo,
        function ($foo_value) use ($bar, $baz) {
          if ($baz[0] instanceof YetAnotherUnusedInterface) {
            return TRUE;
          }
          return $foo_value === $bar;
        }
      );
    }

  }
}
else {
  class Baz extends FooBarAlias implements BarInterface, InterfaceInSameNs {

    use TraitInSameNs;
    use AnotherTraitInSameNs {
      foo as antotherTraitFoo;
    }
    use YetAntotherTrait;

  }
}
EOL
    ,
    'actual' => <<<'EOL'
<?php

namespace Drupal\_original_\foo\Bar;

use Drupal\foo\Bar\FooBazInterface;
use Drupal\foo\Bar\InterfaceInSameNs;
use Drupal\foo\Bar\TraitInSameNs;
use Drupal\foo\Bar\AnotherTraitInSameNs;
use Drupal\foo\Bar\YetAntotherTrait;
use Drupal\foo\Bar\FooBazBarInterface;
use Drupal\foo\Bar\AnotherUnusedClass;
use Drupal\foo\Bar\YetAnother1;
use Drupal\foo\Bar\YetAnother2;
use Drupal\foo\Bar\YetAnotherUnusedInterface;
use Drupal\foo\BarInterface;
use Drupal\foo\FooBar as FooBarAlias;

if (class_exists(FooBazBarInterface::class)) {
  abstract class Baz extends FooBarAlias implements BarInterface, FooBazInterface, InterfaceInSameNs {

    use TraitInSameNs;

    public function superFoo(array $foo, string $bar, AnotherUnusedClass $baz, YetAnother1|YetAnother2|iterable $union): array {
      return array_filter(
        $foo,
        function ($foo_value) use ($bar, $baz) {
          if ($baz[0] instanceof YetAnotherUnusedInterface) {
            return TRUE;
          }
          return $foo_value === $bar;
        }
      );
    }

  }
}
else {
  abstract class Baz extends FooBarAlias implements BarInterface, InterfaceInSameNs {

    use TraitInSameNs;
    use AnotherTraitInSameNs {
      foo as antotherTraitFoo;
    }
    use YetAntotherTrait;

  }
}
EOL
    ,
  ];

}
