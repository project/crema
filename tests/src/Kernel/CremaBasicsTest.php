<?php

namespace Drupal\Tests\crema\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\migrate\Plugin\MigrationPluginManager as MigrateMigrationManager;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate_drupal\MigrationPluginManager as MigrateDrupalMigrationManager;

/**
 * Tests basic class replacement functionality.
 *
 * @group crema
 */
class CremaBasicsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'crema',
    'crema_migrate_test',
    'migrate',
  ];

  /**
   * Tests that the default migration plugin manager class gets replaced.
   */
  public function testMigrationPluginManagerClassReplacement(): void {
    $migration_manager = $this->container->get('plugin.manager.migration');
    $this->assertInstanceOf(MigrationPluginManagerInterface::class, $migration_manager);
    $this->assertInstanceOf(MigrateMigrationManager::class, $migration_manager);

    $this->assertTrue(method_exists($migration_manager, 'foo'));
    $this->assertSame('foo', $migration_manager->foo());
  }

  /**
   * Tests that the overridden migration plugin manager class is also replaced.
   */
  public function testAliasedMigrationPluginManagerClassReplacement(): void {
    $this->enableModules(['migrate_drupal']);

    $this->testMigrationPluginManagerClassReplacement();

    $migration_manager = $this->container->get('plugin.manager.migration');
    $this->assertInstanceOf(MigrateDrupalMigrationManager::class, $migration_manager);

    $this->assertTrue(method_exists($migration_manager, 'bar'));
    $this->assertSame('bar', $migration_manager->bar());
  }

}
