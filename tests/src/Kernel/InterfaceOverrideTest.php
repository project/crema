<?php

namespace Drupal\Tests\crema\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests how overridden interfaces affect their implementers.
 *
 * @group crema
 */
class InterfaceOverrideTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'crema',
    'crema_interface_override_test',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
  }

  /**
   * Tests that an interface and its class can be overridden.
   */
  public function testInterfaceOverride(): void {
    $user = $this->createUser([], NULL, FALSE, [
      'init' => 'init@localhost',
      'mail' => 'mail@localhost',
    ]);
    $this->assertIsCallable([$user, 'forgetInitialEmail']);
    $this->assertEquals('init@localhost', $user->getInitialEmail());

    $user->forgetInitialEmail();
    $this->assertEquals('mail@localhost', $user->getInitialEmail());
  }

}
