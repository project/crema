<?php

namespace Drupal\Tests\crema\Functional;

use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests basic class replacement functionality.
 *
 * @group crema
 */
class CremaTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'crema_migrate_test',
  ];

  /**
   * Tests that migration plugin manager doesn't have a foo method by default.
   */
  public function testMigrationPluginManagerDefault(): void {
    $installer = \Drupal::service('module_installer');
    assert($installer instanceof ModuleInstallerInterface);
    $installer->install(['migrate']);

    $migration_manager = \Drupal::getContainer()->get('plugin.manager.migration');
    $this->assertInstanceOf(MigrationPluginManagerInterface::class, $migration_manager);
    $this->assertInstanceOf('\Drupal\migrate\Plugin\MigrationPluginManager', $migration_manager);

    $this->assertFalse(method_exists($migration_manager, 'foo'));
  }

  /**
   * Tests that the default migration plugin manager class gets replaced.
   *
   * @depends testMigrationPluginManagerDefault
   */
  public function testMigrationPluginManagerClassReplacement(): void {
    $installer = \Drupal::service('module_installer');
    assert($installer instanceof ModuleInstallerInterface);
    $installer->install(['crema', 'migrate']);

    $migration_manager = \Drupal::getContainer()->get('plugin.manager.migration');
    $this->assertInstanceOf(MigrationPluginManagerInterface::class, $migration_manager);
    $this->assertInstanceOf('\Drupal\migrate\Plugin\MigrationPluginManager', $migration_manager);

    $this->assertTrue(method_exists($migration_manager, 'foo'));
    $this->assertSame('foo', $migration_manager->foo());
  }

  /**
   * Tests that the overridden migration plugin manager class is also replaced.
   *
   * @depends testMigrationPluginManagerClassReplacement
   */
  public function testAliasedMigrationPluginManagerClassReplacement(): void {
    $this->testMigrationPluginManagerClassReplacement();

    $installer = \Drupal::service('module_installer');
    assert($installer instanceof ModuleInstallerInterface);
    $installer->install(['migrate_drupal']);

    $migration_manager = \Drupal::getContainer()->get('plugin.manager.migration');
    $this->assertInstanceOf(MigrationPluginManagerInterface::class, $migration_manager);
    $this->assertInstanceOf('\Drupal\migrate_drupal\MigrationPluginManager', $migration_manager);

    $this->assertTrue(method_exists($migration_manager, 'foo'));
    $this->assertSame('foo', $migration_manager->foo());

    $this->assertTrue(method_exists($migration_manager, 'bar'));
    $this->assertSame('bar', $migration_manager->bar());
  }

}
