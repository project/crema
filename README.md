INTRODUCTION
------------

A PoC module which aim to "replace" PHP classes you define in your module's
info.yml file.


REQUIREMENTS
------------

TBD.


INSTALLATION
------------

You can install this module as you would normally install any other contributed
Drupal 9+ module.


CONFIGURATION
-------------

This module does not have any configuration option.


USAGE
-----

TBD.


MAINTAINERS
-----------

* Zoltán Horváth (huzooka) - https://www.drupal.org/u/huzooka
